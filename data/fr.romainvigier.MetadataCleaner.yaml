# SPDX-FileCopyrightText: 2020 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

app-id: fr.romainvigier.MetadataCleaner
runtime: org.gnome.Platform
runtime-version: '3.38'
sdk: org.gnome.Sdk
command: metadata-cleaner
finish-args:
  - --socket=wayland
  - --socket=fallback-x11
  - --share=ipc
  - --device=dri

cleanup:
  - /include
  - /lib/pkgconfig
  - /share/doc
  - /share/man
  - '*.a'
  - '*.la'

modules:
  - name: libhandy
    buildsystem: meson
    config-opts:
      - "-Dtests=false"
      - "-Dexamples=false"
      - "-Dglade_catalog=disabled"
      - "-Dvapi=false"
    sources:
      - type: git
        url: https://gitlab.gnome.org/GNOME/libhandy.git
        tag: 1.0.2
        commit: 465c00f8f80c27330be494ed7c0ba2ffe26321c4

  - name: ffmpeg
    buildsystem: autotools
    sources:
      - type: git
        url: https://git.ffmpeg.org/ffmpeg.git
        tag: n4.3.1
        commit: 6b6b9e593dd4d3aaf75f48d40a13ef03bdef9fdb

  - name: perl
    buildsystem: simple
    build-commands:
      - ./Configure -des -Dprefix=$FLATPAK_DEST
      - make
      - make install
    post-install:
      # Fix wrong permissions
      - chmod -R u+w $FLATPAK_DEST/lib/perl5
    sources:
      - type: archive
        url: https://www.cpan.org/src/5.0/perl-5.32.0.tar.gz
        sha256: efeb1ce1f10824190ad1cadbcccf6fdb8a5d37007d0100d2d9ae5f2b5900c0b4

  - name: exiftool
    buildsystem: simple
    build-commands:
      - perl Makefile.PL
      - make install
    sources:
      - type: archive
        url: https://exiftool.org/Image-ExifTool-12.12.tar.gz
        sha256: 4a6536c69c86a0c87b8eed355938ba3eb903324d2f3aea5e6e87d91172b8ecc8

  - name: poppler
    buildsystem: cmake
    sources:
      - type: git
        url: https://anongit.freedesktop.org/git/poppler/poppler.git
        tag: poppler-20.12.0
        commit: 8766a45a1e101538724d2b767b3efd0ec210357c

  - name: python3-mutagen
    buildsystem: simple
    build-commands:
      - python3 setup.py build
      - python3 setup.py install --optimize=1 --single-version-externally-managed --prefix=$FLATPAK_DEST --root=/
    ensure-writable:
      - /lib/python3*/site-packages/easy-install.pth
    sources:
      - type: git
        url: https://github.com/quodlibet/mutagen.git
        tag: release-1.45.1
        commit: 07108702b18d373d883ee9a36d2dd1066e621a0c

  - name: python3-libmat2
    buildsystem: simple
    build-commands:
      - python3 setup.py build
      - python3 setup.py install --optimize=1 --single-version-externally-managed --prefix=$FLATPAK_DEST --root=/
    ensure-writable:
      - /lib/python3*/site-packages/easy-install.pth
    sources:
      - type: git
        url: https://0xacab.org/jvoisin/mat2.git
        branch: master

  - name: mimetypes
    buildsystem: simple
    build-commands:
      - install -D mime.types $FLATPAK_DEST/share/
    sources:
      - type: file
        url: https://bazaar.launchpad.net/~ubuntu-branches/debian/stretch/mime-support/stretch/download/head:/mime.types
        sha256: f0552ed253ba5e462a4e0ff0dcda3615087a97cb94774faef1f86860f1380a85

  - name: metadata-cleaner
    buildsystem: meson
    sources:
      - type: dir
        path: ../
