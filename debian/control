Source: metadata-cleaner
Section: utils
Priority: optional
Maintainer: Peymaneh Nejad <p.nejad@posteo.de>
Build-Depends: appstream,
               debhelper-compat (= 13),
               dh-python,
               libgtk-3-dev (>= 3.24.0),
               libhandy-1-dev,
               mat2,
               meson (>= 0.51.0),
               pkg-config,
               python-gi-dev,
               python3-dev,
               python3-setuptools
Standards-Version: 4.5.1
Homepage: https://gitlab.com/rmnvgr/metadata-cleaner
Vcs-Git: https://salsa.debian.org/peymaneh/metadata-cleaner.git
Vcs-Browser: https://salsa.debian.org/peymaneh/metadata-cleaner
Rules-Requires-Root: no

Package: metadata-cleaner
Architecture: any
Depends: gir1.2-handy-1,
         libgtk-3-0 (>= 3.24.0),
         mat2,
         mime-support,
         ${misc:Depends},
         ${python3:Depends},
Description: GTK app for viewing and cleaning metadata in files
 Metadata within a file can tell a lot about you. Cameras record data about when
 a picture was taken and what camera was used. Office applications automatically
 add author and company information to documents and spreadsheets. Maybe you
 do not want to disclose those information.
 .
 This tool allows you to view metadata in your files and to get rid of them as
 much as possible.
 .
 Under the hood, it relies on mat2 to parse and remove the metadata.
